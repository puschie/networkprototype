#include "NetworkPrototype.h"
#include "PlayerCharacter.h"

class AProjectile;
class ANetworkPrototypeGameMode;

APlayerCharacter::APlayerCharacter()
{
 	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = 0.5f;

	GetCapsuleComponent()->InitCapsuleSize( 52.f, 52.f );
	GetCapsuleComponent()->BodyInstance.SetCollisionProfileName( "PhysicsActor" );
	GetCapsuleComponent()->SetNotifyRigidBodyCollision( true );
	GetCapsuleComponent()->OnComponentHit.AddDynamic( this, &APlayerCharacter::OnHit );

	const ConstructorHelpers::FObjectFinder<UStaticMesh> DefaultBallMesh( TEXT( "/Engine/BasicShapes/Sphere" ) );

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>( TEXT( "StaticMesh" ) );
	MeshComponent->AttachTo( RootComponent );
	MeshComponent->SetStaticMesh( DefaultBallMesh.Object );

	CameraComponent = CreateDefaultSubobject<UCameraComponent>( TEXT( "PlayerCamera" ) );
	CameraComponent->AttachTo( RootComponent );
	CameraComponent->bUsePawnControlRotation = false;
	CameraComponent->SetRelativeLocation( FVector( 0.f, 0.f, 400.f ) );
	CameraComponent->SetRelativeRotation( FRotator( -90.f, 0.f, 0.f ) );

	RendererComponent = CreateAbstractDefaultSubobject<UTextRenderComponent>( TEXT( "TextRenderer" ) );
	RendererComponent->AttachTo( RootComponent );
	RendererComponent->SetRelativeLocation( FVector( 0.f, 0.f, 50.f ) );
	RendererComponent->SetRelativeRotation( FRotator( 90.f, 0.f, 180.f ) );

	// Disable Collision on other Elements
	MeshComponent->BodyInstance.SetCollisionProfileName( "NoCollision" );
	GetMesh()->BodyInstance.SetCollisionProfileName( "NoCollision" );

	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	bReplicates = true;
	bReplicateMovement = true;

	ActionRate = 1.5f;
	bCanFire = false;
	Offset = FVector( 100.f, 100.f, 0.f );
	MaterialCache = NULL;
	Life = 10;
}

void APlayerCharacter::GetLifetimeReplicatedProps( TArray<FLifetimeProperty>& OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME( APlayerCharacter, Life );
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer( Cooldown, this, &APlayerCharacter::LateInit, 0.1f, false );
}

void APlayerCharacter::Tick( float DeltaTime )
{
	UpdateRenderer();
}

void APlayerCharacter::SetupPlayerInputComponent( class UInputComponent* InputComponent )
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAxis( "Right", this, &APlayerCharacter::MoveRight );
	InputComponent->BindAxis( "Up", this, &APlayerCharacter::MoveUp );

	InputComponent->BindAction( "Action", IE_Pressed, this, &APlayerCharacter::Action );
}

void APlayerCharacter::CooldownEnd()
{
	bCanFire = true;
}

void APlayerCharacter::CooldownStart()
{
	bCanFire = false;
	GetWorld()->GetTimerManager().SetTimer( Cooldown, this, &APlayerCharacter::CooldownEnd, ActionRate, false );
}

void APlayerCharacter::LateInit()
{
	GetPlayerState();

	UpdateRenderer();

	CooldownStart();
}

void APlayerCharacter::MoveUp( float Value )
{
	if( Controller != NULL && Value != 0.f )
	{
		const FRotator YawRotation( 0.f, Controller->GetControlRotation().Yaw, 0.f );
		const FVector Direction = FRotationMatrix( YawRotation ).GetUnitAxis( EAxis::X );
		AddMovementInput( Direction, Value );
	}
}

void APlayerCharacter::MoveRight( float Value )
{
	if( Controller != NULL && Value != 0.f )
	{
		const FRotator YawRotation( 0.f, Controller->GetControlRotation().Yaw, 0.f );
		const FVector Direction = FRotationMatrix( YawRotation ).GetUnitAxis( EAxis::Y );
		AddMovementInput( Direction, Value );
	}
}

void APlayerCharacter::OnHit( AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit )
{
	if( OtherActor != NULL &&
		OtherActor != this &&
		OtherComp != NULL &&
		GetPlayerState() )
	{
		AProjectile* HitObject = Cast<AProjectile>( OtherActor );
		
		if( HitObject && HitObject->GetOwnerState()->PlayerId != GetPlayerState()->PlayerId )
		{
			
			if( --Life <= 0 ) // Prevent server lag
			{
				Destroy();
			}

			if( HasAuthority() )
			{
				if( Life <= 0 )
				{
					ANetworkPrototypeGameMode* GameModeInstance = Cast<ANetworkPrototypeGameMode>( GetWorld()->GetAuthGameMode() );
					
					if( GameModeInstance )
					{
						GameModeInstance->CheckLife( Cast<APlayerController>( GetController() ), Life );
					}
				}
				else
				{
					GetPlayerState()->AddPoints( -1 );
				}
			}
		}
	}
}

void APlayerCharacter::Destroyed()
{
	if( GetPlayerState() &&
		HasAuthority() )
	{
		ANetworkPrototypeGameMode* GameModeInstance = Cast<ANetworkPrototypeGameMode>( GetWorld()->GetAuthGameMode() );

		GetPlayerState()->AddPoints( -5 );

		if( GameModeInstance )
		{
			GameModeInstance->CheckLife( Cast<APlayerController>( GetController() ), 0 );
		}
	}

	Super::Destroyed();
}

AExtendedPlayerState* APlayerCharacter::GetPlayerState()
{
	if( !ExtendedPlayerState && PlayerState )
	{
		ExtendedPlayerState = Cast<AExtendedPlayerState>( PlayerState );
	}
	return ExtendedPlayerState;
}

void APlayerCharacter::Action_Implementation()
{
	if( bCanFire )
	{
		FVector SpawnLocation = ( ExtendedPlayerState->GetPlayerNumber() % 2 ) ? ( GetActorLocation() + Offset ) : ( GetActorLocation() - Offset );
		AProjectile* ActionObject = Cast<AProjectile>( GetWorld()->SpawnActor<AActor>( AProjectile::StaticClass(), SpawnLocation, GetControlRotation() ) );
		
		if( MaterialCache == NULL )
		{
			FString Path = FString( "/Engine/EngineDebugMaterials/" ) + ExtendedPlayerState->GetColorMaterialName();
			MaterialCache = Cast<UMaterial>( StaticLoadObject( UMaterial::StaticClass(), NULL, *Path ) );
		}
		
		ActionObject->SetMaterial( MaterialCache );
		ActionObject->SetOwnerState( ExtendedPlayerState );

		CooldownStart();
	}
}

bool APlayerCharacter::Action_Validate()
{
	return true;
}

void APlayerCharacter::UpdateRenderer()
{
	if( ExtendedPlayerState )
	{
		RendererComponent->SetText( FText::Format( NSLOCTEXT( "", "", "{0}\nLife {1}\nPoints {2}" ), FText::FromString( PlayerState->PlayerName ), FText::AsNumber( Life ), FText::AsNumber( GetPlayerState()->GetPoints() ) ) );
	}
	else
	{
		RendererComponent->SetText( FText::FromString( "Player\n0" ) );
	}
}

