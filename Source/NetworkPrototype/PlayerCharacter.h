#pragma once

#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class AExtendedPlayerState;

UCLASS()
class NETWORKPROTOTYPE_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = ( AllowPrivateAccess = "true" ) )
	class UCameraComponent* CameraComponent;

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = Mesh, meta = ( AllowPrivateAccess = "true" ) )
	class UStaticMeshComponent* MeshComponent;

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = UI, meta = ( AllowPrivateAccess = "true" ) )
	class UTextRenderComponent* RendererComponent;

	FTimerHandle Cooldown;

	AExtendedPlayerState* ExtendedPlayerState;

	float ActionRate;

	bool bCanFire;

	FVector Offset;

	UMaterial* MaterialCache;

	UPROPERTY( ReplicatedUsing=UpdateRenderer )
	float Life;

public:
	APlayerCharacter();

	virtual void GetLifetimeReplicatedProps( TArray<FLifetimeProperty>& OutLifetimeProps ) const;

	virtual void BeginPlay() override;

	virtual void Tick( float DeltaTime ) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	FORCEINLINE class UCameraComponent* GetCamera() const
	{
		return CameraComponent;
	}

	FORCEINLINE class UStaticMeshComponent* GetStaticMesh() const
	{
		return MeshComponent;
	}

	FORCEINLINE class UTextRenderComponent* GetRendereComponent() const
	{
		return RendererComponent;
	}

	UFUNCTION()
	void UpdateRenderer();

private:

	void CooldownEnd();
	void CooldownStart();

	void LateInit();

	void MoveUp( float Value );

	void MoveRight( float Value );

	UFUNCTION( Server, Reliable, WithValidation )
	void Action();

	UFUNCTION()
	void OnHit( AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit );

	virtual void Destroyed() override;

	AExtendedPlayerState* GetPlayerState();
};
