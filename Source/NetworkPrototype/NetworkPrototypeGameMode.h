#pragma once

#include "GameFramework/GameMode.h"
#include "NetworkPrototypeGameMode.generated.h"

/**
 * 
 */
UCLASS()
class NETWORKPROTOTYPE_API ANetworkPrototypeGameMode : public AGameMode
{
	GENERATED_BODY()

	ANetworkPrototypeGameMode();

	void RespawnPlayer( APlayerController* PlayerControler );
	void EndGame();

private:
	FTimerHandle	RespawnTimer;
	
public:
	void CheckLife( APlayerController* PlayerController, float Life );
	void CheckPoints( APlayerController* PlayerController, float Points );
};
