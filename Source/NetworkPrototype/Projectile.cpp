#include "NetworkPrototype.h"
#include "Projectile.h"

class APlayerCharacter;

AProjectile::AProjectile()
{
	PrimaryActorTick.bCanEverTick = false;

	const ConstructorHelpers::FObjectFinder<UStaticMesh> DefaultBallMesh( TEXT( "/Engine/BasicShapes/Sphere" ) );

	GetStaticMeshComponent()->SetStaticMesh( DefaultBallMesh.Object );
	GetStaticMeshComponent()->SetRelativeScale3D( FVector( 0.5f, 0.5f, 0.5f ) );
	GetStaticMeshComponent()->BodyInstance.SetCollisionProfileName( "Projectile" );
	GetStaticMeshComponent()->SetNotifyRigidBodyCollision( true ); // Generate Hit Event
	GetStaticMeshComponent()->OnComponentHit.AddDynamic( this, &AProjectile::OnHit );
	GetStaticMeshComponent()->SetMobility( EComponentMobility::Movable );
		
	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>( TEXT( "MovementComponent" ) );
	MovementComponent->UpdatedComponent = GetStaticMeshComponent();
	MovementComponent->InitialSpeed = 1000.f;
	MovementComponent->MaxSpeed = 1000.f;
	MovementComponent->bRotationFollowsVelocity = true;

	bReplicates = true;
	bReplicateMovement = true;

	OwningPlayerState = NULL;

	InitialLifeSpan = 5.f;
}

void AProjectile::GetLifetimeReplicatedProps( TArray<FLifetimeProperty>& OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME( AProjectile, OwningPlayerState )
}

void AProjectile::SetOwnerState( AExtendedPlayerState* PlayerState )
{
	OwningPlayerState = PlayerState;
}

void AProjectile::SetMaterial_Implementation( UMaterial* NewMaterial )
{
	GetStaticMeshComponent()->SetMaterial( 0, NewMaterial );
}

void AProjectile::OnHit( AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit )
{
	if( OtherActor != NULL &&
		OtherActor != this &&
		OtherComp != NULL )
	{
		if( OtherActor->IsA( APlayerCharacter::StaticClass() ) )
		{
			if( HasAuthority() )
			{
				APlayerCharacter* HitTargetActor = Cast<APlayerCharacter>( OtherActor );

				if( HitTargetActor->PlayerState->PlayerId != OwningPlayerState->PlayerId )
				{
					OwningPlayerState->AddPoints( 2 );
				}
			}
			Destroy();
		}
	}
}
