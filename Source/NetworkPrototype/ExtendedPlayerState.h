#pragma once

#include "GameFramework/PlayerState.h"
#include "ExtendedPlayerState.generated.h"

class ANetworkPrototypeGameMode;

UCLASS()
class NETWORKPROTOTYPE_API AExtendedPlayerState : public APlayerState
{
	GENERATED_BODY()

	UPROPERTY( Replicated )
	int32 Points;

	UPROPERTY( Replicated )
	FString ColorMaterialName;

	UPROPERTY( Replicated )
	int32 PlayerNumber;

	ANetworkPrototypeGameMode* GameModeCache;
	APlayerController* PlayerControllerCache;

public:
	AExtendedPlayerState();

	virtual void GetLifetimeReplicatedProps( TArray<FLifetimeProperty>& OutLifetimeProps ) const;

	void AddPoints( int32 Value );

	UFUNCTION( BlueprintCallable, Category = Extended )
	void SetColor( int32 NumberOfPlayer );

	FORCEINLINE int32 GetPoints()
	{
		return Points;
	}

	FORCEINLINE FString GetColorMaterialName()
	{
		return ColorMaterialName;
	}

	FORCEINLINE int32 GetPlayerNumber()
	{
		return PlayerNumber;
	}

private:
	ANetworkPrototypeGameMode* GetGameMode();
	APlayerController* GetPlayerController();
};
