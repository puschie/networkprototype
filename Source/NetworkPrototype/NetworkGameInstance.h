#pragma once

#include "Engine/GameInstance.h"
#include "NetworkGameInstance.generated.h"

UCLASS()
class NETWORKPROTOTYPE_API UNetworkGameInstance : public UGameInstance
{
	GENERATED_BODY()

	// Delegates
	FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDeletage;
	FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;
	FOnFindSessionsCompleteDelegate OnFindSessionsCompleteDelegate;
	FOnJoinSessionCompleteDelegate OnJoinSessionCompleteDelegate;
	FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;

	// Delegate Handle
	FDelegateHandle OnCreateSessionCompleteDelegateHandle;
	FDelegateHandle OnStartSessionCompleteDelegateHandle;
	FDelegateHandle OnFindSessionsCompleteDelegateHandle;
	FDelegateHandle OnJoinSessionCompleteDelegateHandle;
	FDelegateHandle OnDestroySessionCompleteDelegateHandle;

	TSharedPtr<FOnlineSessionSettings> SessionSettings;
	TSharedPtr<FOnlineSessionSearch> SessionSearch;

	IOnlineSubsystem* OnlineSystemCache;
	IOnlineSessionPtr SessionCache;

	bool bSearchComplete;
	TSharedPtr<const FUniqueNetId> SearcherNetId;

public:
	
	UNetworkGameInstance( const FObjectInitializer& ObjectInitializer );

	// Presence == Lobby
	bool HostSessions( TSharedPtr<const FUniqueNetId> UserID, FName SessionName, FString& MapName, bool bIsLAN = false, bool bIsPublic = true, bool bIsLobbyActive = false, int32 MaxPlayers = 10 );
	bool JoinSessions( TSharedPtr<const FUniqueNetId> UserID, FName SessionName, const FOnlineSessionSearchResult& SearchResult );
	void FindSessions( TSharedPtr<const FUniqueNetId> UserID, bool bIsLAN = false, bool bIsLobbyActive = false );

	// Blueprint Functions
	UFUNCTION( BlueprintCallable, Category = Session )
	void CreateAndHostSession( FString MapName );

	UFUNCTION( BlueprintCallable, Category = Session )
	void SearchSessions();
	
	UFUNCTION( BlueprintCallable, Category = Session )
	void JoinSessionBP( const FBlueprintSessionResult& TargetSession );

	UFUNCTION( BlueprintImplementableEvent, Category = Session, meta = ( DisplayName = "Session Search Finished" ) )
	void SearchComplete( const TArray<FBlueprintSessionResult>& SearchResultArray );

	void OnCreateSessionComplete( FName SessionName, bool bSuccessful );
	void OnStartSessionComplete( FName SessionName, bool bSuccessful );
	void OnFindSessionComplete( bool bSuccessful );
	void OnJoinSessionComplete( FName SessionName, EOnJoinSessionCompleteResult::Type Result );
	void OnDestroySessionComplete( FName SessionName, bool bSuccessful );

private:

	IOnlineSubsystem* GetOnlineSystem();
	IOnlineSessionPtr GetSession();
};
