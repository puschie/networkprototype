#include "NetworkPrototype.h"
#include "NetworkPrototypeGameMode.h"

class APlayerCharacter;
class AExtendedPlayerState;

ANetworkPrototypeGameMode::ANetworkPrototypeGameMode()
{
	DefaultPawnClass = APlayerCharacter::StaticClass();
	PlayerStateClass = AExtendedPlayerState::StaticClass();

	bDelayedStart = true;
}

void ANetworkPrototypeGameMode::RespawnPlayer( APlayerController* PlayerControler )
{
	if( PlayerControler )
	{
		RestartPlayer( PlayerControler );
	}
}

void ANetworkPrototypeGameMode::EndGame()
{
	// Close all Connection & Map
	UGameplayStatics::OpenLevel( GetWorld(), "Map_MM" );
}

void ANetworkPrototypeGameMode::CheckLife( APlayerController* PlayerController, float Value )
{
	if( PlayerController && Value <= 0 )
	{
		FTimerDelegate RespawnDelegate = FTimerDelegate::CreateUObject( this, &ANetworkPrototypeGameMode::RespawnPlayer, PlayerController );
		GetWorldTimerManager().SetTimer( RespawnTimer, RespawnDelegate, 3.f, false );
	}
}

void ANetworkPrototypeGameMode::CheckPoints( APlayerController* PlayerController, float Points )
{
	if( PlayerController && Points >= 4 )
	{
		FString WinMessage = PlayerController->PlayerState->PlayerName + "\nWIN WIN WIN WIN";

		GEngine->AddOnScreenDebugMessage( -1, 30.f, FColor::Red, WinMessage );

		GetWorldTimerManager().SetTimer( RespawnTimer, this, &ANetworkPrototypeGameMode::EndGame, 3.f, false );
	}
}
