#pragma once

#include "Engine/StaticMeshActor.h"
#include "Projectile.generated.h"

UCLASS()
class NETWORKPROTOTYPE_API AProjectile : public AStaticMeshActor
{
	GENERATED_BODY()

	UPROPERTY( Replicated )
	class AExtendedPlayerState* OwningPlayerState;

	UPROPERTY( VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = ( AllowPrivateAccess = "true" ) )
	class UProjectileMovementComponent* MovementComponent;

public:	
	AProjectile();

	virtual void GetLifetimeReplicatedProps( TArray<FLifetimeProperty>& OutLifetimeProps ) const;

	UFUNCTION( NetMulticast, Reliable )
	void SetMaterial( UMaterial* NewMaterial );

	void SetOwnerState( AExtendedPlayerState* PlayerState );

	FORCEINLINE class UProjectileMovementComponent* GetMovementComponent()
	{
		return MovementComponent;
	}

	FORCEINLINE class AExtendedPlayerState* GetOwnerState()
	{
		return OwningPlayerState;
	}
private:

	UFUNCTION()
	void OnHit( AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit );
};
