// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class NetworkPrototype : ModuleRules
{
	public NetworkPrototype(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "OnlineSubsystem", "OnlineSubsystemUtils" });

		PrivateDependencyModuleNames.AddRange(new string[] { "OnlineSubsystem" } );

		DynamicallyLoadedModuleNames.Add("OnlineSubsystemNull"); // "OnlineSubsystemSteam" | "OnlineSubsystemNull"
	}
}
