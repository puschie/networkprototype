#include "NetworkPrototype.h"
#include "NetworkGameInstance.h"


UNetworkGameInstance::UNetworkGameInstance(const FObjectInitializer& ObjectInitializer) : Super( ObjectInitializer )
{
	OnCreateSessionCompleteDeletage = FOnCreateSessionCompleteDelegate::CreateUObject( this, &UNetworkGameInstance::OnCreateSessionComplete );
	OnStartSessionCompleteDelegate = FOnStartSessionCompleteDelegate::CreateUObject( this, &UNetworkGameInstance::OnStartSessionComplete );
	OnFindSessionsCompleteDelegate = FOnFindSessionsCompleteDelegate::CreateUObject( this, &UNetworkGameInstance::OnFindSessionComplete );
	OnJoinSessionCompleteDelegate = FOnJoinSessionCompleteDelegate::CreateUObject( this, &UNetworkGameInstance::OnJoinSessionComplete );
	OnDestroySessionCompleteDelegate = FOnDestroySessionCompleteDelegate::CreateUObject( this, &UNetworkGameInstance::OnDestroySessionComplete );

	OnlineSystemCache = nullptr;

	bSearchComplete = true;
}

bool UNetworkGameInstance::HostSessions( TSharedPtr<const FUniqueNetId> UserID, FName SessionName, FString& MapName, bool bIsLAN /*= false*/, bool bIsPublic /*= true*/, bool bIsLobbyActive /*= false*/, int32 MaxPlayers /*= 10 */ )
{
	if( GetSession().IsValid() && UserID.IsValid() )
	{
		SessionSettings = MakeShareable( new FOnlineSessionSettings() );

		SessionSettings->bIsLANMatch = bIsLAN;
		SessionSettings->bUsesPresence = bIsLobbyActive;
		SessionSettings->NumPublicConnections = ( bIsPublic ) ? ( MaxPlayers ) : ( 0 );
		SessionSettings->NumPrivateConnections = ( bIsPublic ) ? ( 0 ) : ( MaxPlayers );
		SessionSettings->bAllowInvites = true;
		SessionSettings->bAllowJoinInProgress = true;
		SessionSettings->bShouldAdvertise = bIsPublic;
		SessionSettings->bAllowJoinViaPresence = bIsLobbyActive;
		SessionSettings->bAllowJoinViaPresenceFriendsOnly = false;

		SessionSettings->Set( SETTING_MAPNAME, MapName, ( bIsPublic ) ? ( EOnlineDataAdvertisementType::DontAdvertise ) : ( EOnlineDataAdvertisementType::ViaOnlineService ) );

		OnCreateSessionCompleteDelegateHandle = GetSession()->AddOnCreateSessionCompleteDelegate_Handle( OnCreateSessionCompleteDeletage );
		
		GetSession()->DestroySession( SessionName );
		
		return GetSession()->CreateSession( *UserID, SessionName, *SessionSettings );
	}
	return false;
}

bool UNetworkGameInstance::JoinSessions( TSharedPtr<const FUniqueNetId> UserID, FName SessionName, const FOnlineSessionSearchResult& SearchResult )
{
	if( GetSession().IsValid() && UserID.IsValid() )
	{
		OnJoinSessionCompleteDelegateHandle = GetSession()->AddOnJoinSessionCompleteDelegate_Handle( OnJoinSessionCompleteDelegate );

		return GetSession()->JoinSession( *UserID, SessionName, SearchResult );
	}
	return false;
}

void UNetworkGameInstance::FindSessions( TSharedPtr<const FUniqueNetId> UserID, bool bIsLAN /*= false*/, bool bIsLobbyActive /*= false */ )
{
	if( GetSession().IsValid() && bSearchComplete )
	{
		bSearchComplete = false;

		SessionSearch = MakeShareable( new FOnlineSessionSearch() );

		SessionSearch->bIsLanQuery = bIsLAN;
		SessionSearch->MaxSearchResults = 20;
		SessionSearch->PingBucketSize = 100;

		if( bIsLobbyActive )
		{
			SessionSearch->QuerySettings.Set( SEARCH_PRESENCE, bIsLobbyActive, EOnlineComparisonOp::Equals );
		}

		TSharedRef<FOnlineSessionSearch> SearchSettingsRef = SessionSearch.ToSharedRef();

		OnFindSessionsCompleteDelegateHandle = GetSession()->AddOnFindSessionsCompleteDelegate_Handle( OnFindSessionsCompleteDelegate );

		SearcherNetId = UserID;

		GetSession()->FindSessions( *UserID, SearchSettingsRef );
	}
}

void UNetworkGameInstance::CreateAndHostSession( FString MapName )
{
	bool Result = HostSessions( GetFirstGamePlayer()->GetPreferredUniqueNetId(), GameSessionName, MapName, true, true, true, 2 );
	UE_LOG( LogTemp, Log, TEXT( "Host Session : %d" ), Result );
}

void UNetworkGameInstance::SearchSessions()
{
	FindSessions( GetFirstGamePlayer()->GetPreferredUniqueNetId(), true, true );
}

void UNetworkGameInstance::JoinSessionBP( const FBlueprintSessionResult& TargetSession )
{
	bool Result = JoinSessions( GetFirstGamePlayer()->GetPreferredUniqueNetId(), GameSessionName, TargetSession.OnlineResult );
	UE_LOG( LogTemp, Log, TEXT( "Join Session : %d" ), Result );
}

void UNetworkGameInstance::OnCreateSessionComplete( FName SessionName, bool bSuccessful )
{
	if( GetSession().IsValid() )
	{
		GetSession()->ClearOnCreateSessionCompleteDelegate_Handle( OnCreateSessionCompleteDelegateHandle );

		if( bSuccessful )
		{
			OnStartSessionCompleteDelegateHandle = GetSession()->AddOnStartSessionCompleteDelegate_Handle( OnStartSessionCompleteDelegate );

			GetSession()->StartSession( SessionName );
		}
	}
}

void UNetworkGameInstance::OnStartSessionComplete( FName SessionName, bool bSuccessful )
{
	if( GetSession().IsValid() )
	{
		GetSession()->ClearOnStartSessionCompleteDelegate_Handle( OnStartSessionCompleteDelegateHandle );
	}

	if( bSuccessful )
	{
		FString MapName;
		if( !SessionSettings->Get( SETTING_MAPNAME, MapName ) )
		{
			MapName = "Map_B";
		}
		UGameplayStatics::OpenLevel( GetWorld(), *MapName, true, "listen" );
	}
}

void UNetworkGameInstance::OnFindSessionComplete( bool bSuccessful )
{
	if( GetSession().IsValid() )
	{
		GetSession()->ClearOnFindSessionsCompleteDelegate_Handle( OnFindSessionsCompleteDelegateHandle );

		// Copy Search Result into blueprint struct
		auto ResultArray = SessionSearch->SearchResults;
		auto ResultIter = ResultArray.CreateIterator();
		
		TArray<FBlueprintSessionResult> SearchResultArray;
		SearchResultArray.SetNum( ResultArray.Num() );

		for( FBlueprintSessionResult& SearchResult : SearchResultArray )
		{
			if( SearcherNetId.IsValid() && SearcherNetId != ResultIter->Session.OwningUserId ) // Prevent finding own session
			{
				SearchResult.OnlineResult = *ResultIter;
			}
			ResultIter++;
		}
		SearchComplete( SearchResultArray );

		bSearchComplete = true;
	}
}

void UNetworkGameInstance::OnJoinSessionComplete( FName SessionName, EOnJoinSessionCompleteResult::Type Result )
{
	if( GetSession().IsValid() )
	{
		FString TravelURL;

		GetSession()->ClearOnJoinSessionCompleteDelegate_Handle( OnJoinSessionCompleteDelegateHandle );

		if( GetFirstLocalPlayerController() && GetSession()->GetResolvedConnectString( SessionName, TravelURL ) )
		{
			GetFirstLocalPlayerController()->ClientTravel( TravelURL, ETravelType::TRAVEL_Absolute );
		}
	}
}

void UNetworkGameInstance::OnDestroySessionComplete( FName SessionName, bool bSuccessful )
{
	if( GetSession().IsValid() )
	{
		GetSession()->ClearOnDestroySessionCompleteDelegate_Handle( OnDestroySessionCompleteDelegateHandle );

		if( bSuccessful )
		{
			UGameplayStatics::OpenLevel( GetWorld(), "Map_MM", true );
		}
	}
}

IOnlineSubsystem* UNetworkGameInstance::GetOnlineSystem()
{
	
	if( !OnlineSystemCache )
	{
		OnlineSystemCache = IOnlineSubsystem::Get(); // FName("Steam")
	}
	return OnlineSystemCache;
}

IOnlineSessionPtr UNetworkGameInstance::GetSession()
{
	if( !SessionCache.IsValid() )
	{
		if( GetOnlineSystem() )
		{
			SessionCache = GetOnlineSystem()->GetSessionInterface();
		}
	}
	return SessionCache;
}
