#include "NetworkPrototype.h"
#include "ExtendedPlayerState.h"

AExtendedPlayerState::AExtendedPlayerState()
{
	Points = 0;
	ColorMaterialName = FString( "VertexColorViewMode_ColorOnly" );

	GameModeCache = nullptr;
	PlayerControllerCache = nullptr;
}

void AExtendedPlayerState::GetLifetimeReplicatedProps( TArray<FLifetimeProperty>& OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME( AExtendedPlayerState, Points );

	DOREPLIFETIME( AExtendedPlayerState, ColorMaterialName );

	DOREPLIFETIME( AExtendedPlayerState, PlayerNumber );
}

void AExtendedPlayerState::AddPoints( int32 Value )
{
	Points += Value;

	GetGameMode()->CheckPoints( GetPlayerController(), Points );
}

void AExtendedPlayerState::SetColor( int32 Value )
{
	switch( Value )
	{
		case 1:
			ColorMaterialName = FString( "VertexColorViewMode_BlueOnly" );
			break;
		case 2:
			ColorMaterialName = FString( "VertexColorViewMode_ColorOnly" );
			break;
		case 3:
			ColorMaterialName = FString( "VertexColorViewMode_GreenOnly" );
			break;
		case 4:
			ColorMaterialName = FString( "VertexColorViewMode_RedOnly" );
			break;
		default:
			ColorMaterialName = FString( "VertexColorViewMode_ColorOnly" );
			break;
	}
	PlayerNumber = Value;
}

ANetworkPrototypeGameMode* AExtendedPlayerState::GetGameMode()
{
	if( !GameModeCache )
	{
		GameModeCache = Cast<ANetworkPrototypeGameMode>( GetWorld()->GetAuthGameMode() );
	}
	return GameModeCache;
}

APlayerController* AExtendedPlayerState::GetPlayerController()
{
	if( !PlayerControllerCache )
	{
		for( FConstPlayerControllerIterator PlayerControllerIterator = GetWorld()->GetPlayerControllerIterator(); PlayerControllerIterator; PlayerControllerIterator++ )
		{
			if( ( *PlayerControllerIterator )->PlayerState == this )
			{
				PlayerControllerCache = Cast<APlayerController>( *PlayerControllerIterator );
				break;
			}
		}
	}
	return PlayerControllerCache;
}
